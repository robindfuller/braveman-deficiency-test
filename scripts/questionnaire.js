/**
 * Created by robinfuller on 30/10/14.
 */

var app = angular.module("app", []);

app.controller("questionnaire", function($scope) {

    $scope.stages = [
        {id:1, name:'Dopamine'},
        {id:2, name:'Acetylcholine'},
        {id:3, name:'GABA'},
        {id:4, name:'Serotonin'}
    ];

    $scope.categories = [
        {id:1, name:'Memory and Attention'},
        {id:2, name:'Physical'},
        {id:3, name:'Personality'},
        {id:4, name:'Character'}
    ];

    $scope.totalPages = $scope.stages.length * $scope.categories.length;
    $scope.currentCategory = 0;
    $scope.currentStage = 0;

    $scope.questions = [
        {s:1,c:1,a:null,t:'I have trouble paying consistent attention and concentrating'},
        {s:1,c:1,a:null,t:'I need caffeine to wake up'},
        {s:1,c:1,a:null,t:'I cannot think quickly enough'},
        {s:1,c:1,a:null,t:'I do not have a good attention span'},
        {s:1,c:1,a:null,t:'I have trouble getting through a task even when it is interesting to me'},
        {s:1,c:1,a:null,t:'I am slow in learning new ideas'},
        {s:1,c:2,a:null,t:'I crave sugar'},
        {s:1,c:2,a:null,t:'I have decreased libido'},
        {s:1,c:2,a:null,t:'I sleep too much'},
        {s:1,c:2,a:null,t:'I have a history of alcohol or addiction'},
        {s:1,c:2,a:null,t:'I have recently felt worn out for no apparent reason'},
        {s:1,c:2,a:null,t:'I sometimes experience total exhaustion without even exerting myself'},
        {s:1,c:2,a:null,t:'I have always battled weight problems'},
        {s:1,c:2,a:null,t:'I have little motivation for sexual experiences'},
        {s:1,c:2,a:null,t:'I have trouble getting out of bed in the morning'},
        {s:1,c:2,a:null,t:'I have had a craving for cocaine, amphetamines, or Ecstasy'},
        {s:1,c:3,a:null,t:'I feel fine just following others'},
        {s:1,c:3,a:null,t:'People seem to take advantage of me'},
        {s:1,c:3,a:null,t:'I am feeling very down or depressed'},
        {s:1,c:3,a:null,t:'People have told me I am too mellow'},
        {s:1,c:3,a:null,t:'I have little urgency'},
        {s:1,c:3,a:null,t:'I let people criticize me'},
        {s:1,c:3,a:null,t:'I always look to others to lead me'},
        {s:1,c:4,a:null,t:'I have lost my reasoning skills'},
        {s:1,c:4,a:null,t:'I can\'t make good decisions'},
        {s:2,c:1,a:null,t:'I lack imagination.'},
        {s:2,c:1,a:null,t:'I have difficulty remembering names when I first meet people'},
        {s:2,c:1,a:null,t:'I have noticed that my memory ability is decreasing'},
        {s:2,c:1,a:null,t:'My significant other tells me I don\'t have romantic thoughts'},
        {s:2,c:1,a:null,t:'I can\'t remember my friends\' birthdays'},
        {s:2,c:1,a:null,t:'I have lost some of my creativity'},
        {s:2,c:2,a:null,t:'I have insomnia'},
        {s:2,c:2,a:null,t:'I have lost muscle tone'},
        {s:2,c:2,a:null,t:'I don\'t exercise anymore'},
        {s:2,c:2,a:null,t:'I crave fatty foods'},
        {s:2,c:2,a:null,t:'I have experimented with hallucinogens or other illicit drugs'},
        {s:2,c:2,a:null,t:'I feel like my body is falling apart'},
        {s:2,c:2,a:null,t:'I can\'t breathe easily'},
        {s:2,c:3,a:null,t:'I don\'t feel joy very often'},
        {s:2,c:3,a:null,t:'I feel despair'},
        {s:2,c:3,a:null,t:'I protect myself from being hurt by others by never telling much about myself'},
        {s:2,c:3,a:null,t:'I find it more comfortable to do things alone rather than in a large group'},
        {s:2,c:3,a:null,t:'Other people get angry about bothersome things than I do.'},
        {s:2,c:3,a:null,t:'I give in easily and tend to be submissive'},
        {s:2,c:3,a:null,t:'I rarely feel passionate about anything'},
        {s:2,c:3,a:null,t:'I like routine'},
        {s:2,c:4,a:null,t:'I don\'t care about anyone\'s stories but mine'},
        {s:2,c:4,a:null,t:'I don\'t pay attention to people\'s feelings'},
        {s:2,c:4,a:null,t:'I don\'t feel buoyant'},
        {s:2,c:4,a:null,t:'I\'m obsessed with my deficiencies'},
        {s:3,c:1,a:null,t:'I find it difficult to concentrate because I\'m nervous and jumpy'},
        {s:3,c:1,a:null,t:'I can\'t remember phone numbers'},
        {s:3,c:1,a:null,t:'I have trouble finding the right words'},
        {s:3,c:1,a:null,t:'I know I am intelligent, but it is hard to show others'},
        {s:3,c:1,a:null,t:'My ability to focus comes and goes'},
        {s:3,c:1,a:null,t:'When I read, I find I have to go back over the same paragraph a few times to absorb the information'},
        {s:3,c:1,a:null,t:'I am a quick thinker but can\'t always say what I mean'},
        {s:3,c:2,a:null,t:'I feel shaky'},
        {s:3,c:2,a:null,t:'I sometimes tremble'},
        {s:3,c:2,a:null,t:'I have frequent backaches and/or headaches'},
        {s:3,c:2,a:null,t:'I tend to have shortness of breath'},
        {s:3,c:2,a:null,t:'I tend to have heart palpitations'},
        {s:3,c:2,a:null,t:'I tend to have cold hands'},
        {s:3,c:2,a:null,t:'I sometimes sweat too much'},
        {s:3,c:2,a:null,t:'I am sometimes dizzy'},
        {s:3,c:2,a:null,t:'I often have muscle tension'},
        {s:3,c:2,a:null,t:'I tend to get butterflies in my stomach'},
        {s:3,c:2,a:null,t:'I crave bitter foods'},
        {s:3,c:2,a:null,t:'I am often nervous'},
        {s:3,c:2,a:null,t:'I like yoga because it helps me to relax'},
        {s:3,c:2,a:null,t:'I often feel fatigued even when I have had a good night\'s sleep'},
        {s:3,c:2,a:null,t:'I overeat'},
        {s:3,c:3,a:null,t:'I have mood swings'},
        {s:3,c:3,a:null,t:'I enjoy doing many things at one time, but I find it difficult to decide what to do first'},
        {s:3,c:3,a:null,t:'I tend to do things just because I think they\'d be fun'},
        {s:3,c:3,a:null,t:'When things are dull, I always try to introduce some excitement'},
        {s:3,c:3,a:null,t:'I tend to be fickle, changing my mood and thoughts frequently.'},
        {s:3,c:3,a:null,t:'I tend to get overly excited about things'},
        {s:3,c:3,a:null,t:'My impulses tend to get me into a lot of trouble'},
        {s:3,c:3,a:null,t:'I tend to be theatrical and draw attention to myself'},
        {s:3,c:3,a:null,t:'I speak my mind no matter what the reaction of others may be'},
        {s:3,c:3,a:null,t:'I sometimes have fits of rage and then feel terribly guilty'},
        {s:3,c:3,a:null,t:'I often tell lies to get out of trouble'},
        {s:3,c:3,a:null,t:'I have always had less interest than the average person in sex'},
        {s:3,c:4,a:null,t:'I don\'t play by the rules anymore'},
        {s:3,c:4,a:null,t:'I have lost many friends'},
        {s:3,c:4,a:null,t:'I can\'t sustain romantic relationships'},
        {s:3,c:4,a:null,t:'I consider the law arbitrary and without reason'},
        {s:3,c:4,a:null,t:'I now consider rules that I used to follow ridiculous'},
        {s:4,c:1,a:null,t:'I am not very perceptive'},
        {s:4,c:1,a:null,t:'I can\'t remember things that I have seen in the past'},
        {s:4,c:1,a:null,t:'I have a slow reaction time'},
        {s:4,c:1,a:null,t:'I have a poor sense of direction'},
        {s:4,c:2,a:null,t:'I have night sweats'},
        {s:4,c:2,a:null,t:'I have insomnia'},
        {s:4,c:2,a:null,t:'I tend to sleep in many different positions in order to feel comfortable'},
        {s:4,c:2,a:null,t:'I always awake early in the morning'},
        {s:4,c:2,a:null,t:'I can\'t relax'},
        {s:4,c:2,a:null,t:'I wake up at least two times per night'},
        {s:4,c:2,a:null,t:'It is difficult for me to fall back asleep when I am awakened'},
        {s:4,c:2,a:null,t:'I crave salt'},
        {s:4,c:2,a:null,t:'I have less energy to exercise'},
        {s:4,c:2,a:null,t:'I am sad'},
        {s:4,c:3,a:null,t:'I have chronic anxiety'},
        {s:4,c:3,a:null,t:'I am easily irritated'},
        {s:4,c:3,a:null,t:'I have thoughts of self-destruction'},
        {s:4,c:3,a:null,t:'I have had suicidal thoughts in my life'},
        {s:4,c:3,a:null,t:'I tend to dwell on ideas too much'},
        {s:4,c:3,a:null,t:'I am sometimes so structured that I become inflexible'},
        {s:4,c:3,a:null,t:'My imagination takes over'},
        {s:4,c:3,a:null,t:'Fear grips me'},
        {s:4,c:4,a:null,t:'I can\'t stop thinking about the meaning of life'},
        {s:4,c:4,a:null,t:'I no longer want to take risks'},
        {s:4,c:4,a:null,t:'The lack of meaning in my life is painful to me'}
    ];

    $scope.ranges = [
        {name:'Minor Deficit',from:0,to:5},
        {name:'Moderate Deficit',from:6,to:15},
        {name:'Minor Deficit',from:15,to:9999},
    ]

    $scope.recommendations = [
        {id:1,s:1,name:'Phenylalanine',dose:[
            {v:'500', l: 'https://www.google.com'},
            {v:'1000', l: 'https://www.microsoft.com'},
            {v:'1000-2000', l: 'https://www.facebook.com'}
        ],unit:'mg'},
        {id:2,s:1,name:'Tyrosine',dose:[{v:'500', l: ''},{v:'1000', l: ''},{v:'1000-2000', l: ''}],unit:'mg'},
        {id:3,s:1,name:'Methionine',dose:[{v:'250', l: ''},{v:'500', l: ''},{v:'1000', l: ''}],unit:'mg'},
        {id:4,s:1,name:'Rhodiola',dose:[{v:'50', l: ''},{v:'100', l: ''},{v:'200', l: ''}],unit:'mg'},
        {id:5,s:1,name:'Pyroxidine',dose:[{v:'5', l: ''},{v:'10', l: ''},{v:'50', l: ''}],unit:'mg'},
        {id:6,s:1,name:'B complex',dose:[{v:'25', l: ''},{v:'50', l: ''},{v:'100', l: ''}],unit:'mg'},
        {id:7,s:1,name:'Phosphatidylserine',dose:[{v:'50', l: ''},{v:'100', l: ''},{v:'200', l: ''}],unit:'mg'},
        {id:8,s:1,name:'Ginko Biloba',dose:[{v:'50', l: ''},{v:'75', l: ''},{v:'100', l: ''}],unit:'mg'},
        {id:9,s:2,name:'Choline (GPC choline)',dose:[{v:'100', l: ''},{v:'200', l: ''},{v:'500', l: ''}],unit:'mg'},
        {id:10,s:2,name:'Phosphatidylcholine',dose:[{v:'500', l: ''},{v:'1000', l: ''},{v:'2000', l: ''}],unit:'mg'},
        {id:11,s:2,name:'Phosphatidylserine',dose:[{v:'50', l: ''},{v:'100', l: ''},{v:'200', l: ''}],unit:'mg'},
        {id:12,s:2,name:'Acetyl-l-carnitine',dose:[{v:'250', l: ''},{v:'500', l: ''},{v:'1000', l: ''}],unit:'mg'},
        {id:13,s:2,name:'DHA (Docosahexaenoic acid)',dose:[{v:'200', l: ''},{v:'500', l: ''},{v:'1000', l: ''}],unit:'mg'},
        {id:15,s:2,name:'Thiamine',dose:[{v:'25', l: ''},{v:'50', l: ''},{v:'100', l: ''}],unit:'mg'},
        {id:16,s:2,name:'Pantothenic Acid',dose:[{v:'25', l: ''},{v:'50', l: ''},{v:'100', l: ''}],unit:'mg'},
        {id:17,s:2,name:'Vitamin B12',dose:[{v:'100', l: ''},{v:'200', l: ''},{v:'500', l: ''}],unit:'mcg'},
        {id:18,s:2,name:'Taurine',dose:[{v:'250', l: ''},{v:'500', l: ''},{v:'1000', l: ''}],unit:'mcg'},
        {id:19,s:2,name:'Huperzine-A',dose:[{v:'50', l: ''},{v:'100', l: ''},{v:'200', l: ''}],unit:'mg'},
        {id:20,s:2,name:'Ginko Biloba',dose:[{v:'50', l: ''},{v:'75', l: ''},{v:'100', l: ''}],unit:'mg'},
        {id:21,s:2,name:'Korean Ginseng',dose:[{v:'100', l: ''},{v:'200', l: ''},{v:'500', l: ''}],unit:'mg'},
        {id:22,s:3,name:'Inositol',dose:[{v:'500', l: ''},{v:'1000', l: ''},{v:'2000', l: ''}],unit:'mg'},
        {id:23,s:3,name:'GABA (not well-absorbed)',dose:[{v:'100', l: ''},{v:'500', l: ''},{v:'1000', l: ''}],unit:'mg'},
        {id:24,s:3,name:'Glutamic Acid',dose:[{v:'250', l: ''},{v:'500', l: ''},{v:'1000', l: ''}],unit:'mg'},
        {id:25,s:3,name:'Melatonin (at night)',dose:[{v:'1', l: ''},{v:'2', l: ''},{v:'3-6', l: ''}],unit:'mg'},
        {id:26,s:3,name:'Thiamine',dose:[{v:'200', l: ''},{v:'400', l: ''},{v:'600', l: ''}],unit:'mg'},
        {id:27,s:3,name:'Niacinamide',dose:[{v:'25', l: ''},{v:'100', l: ''},{v:'500', l: ''}],unit:'mg'},
        {id:28,s:3,name:'Pyridoxine',dose:[{v:'5', l: ''},{v:'10', l: ''},{v:'50', l: ''}],unit:'mg'},
        {id:29,s:3,name:'Valerian root',dose:[{v:'100', l: ''},{v:'200', l: ''},{v:'500', l: ''}],unit:'mg'},
        {id:30,s:3,name:'Passionflower',dose:[{v:'200', l: ''},{v:'500', l: ''},{v:'1000', l: ''}],unit:'mg'},
        {id:31,s:4,name:'Calcium',dose:[{v:'500', l: ''},{v:'750', l: ''},{v:'1000', l: ''}],unit:'mg'},
        {id:32,s:4,name:'Fish oil (pharmaceutical grade)',dose:[{v:'500', l: ''},{v:'1000', l: ''},{v:'2000', l: ''}],unit:'mg'},
        {id:33,s:4,name:'5-HTP (with decarboxylase inhibitor)',dose:[{v:'100', l: ''},{v:'200', l: ''},{v:'400', l: ''}],unit:'mg'},
        {id:34,s:4,name:'Magnesium',dose:[{v:'200', l: ''},{v:'400', l: ''},{v:'600', l: ''}],unit:'mg'},
        {id:35,s:4,name:'Melatonin (at night)',dose:[{v:'1/3', l: ''},{v:'½-2', l: ''},{v:'1-6', l: ''}],unit:'mg'},
        {id:36,s:4,name:'Passionflower',dose:[{v:'200', l: ''},{v:'500', l: ''},{v:'1000', l: ''}],unit:'mg'},
        {id:37,s:4,name:'Pyridoxine',dose:[{v:'5', l: ''},{v:'10', l: ''},{v:'50', l: ''}],unit:'mg'},
        {id:38,s:4,name:'SAM-e',dose:[{v:'50', l: ''},{v:'100', l: ''},{v:'200', l: ''}],unit:'mg'},
        {id:39,s:4,name:'St. John’s Wort',dose:[{v:'200', l: ''},{v:'400', l: ''},{v:'600', l: ''}],unit:'mg'},
        {id:40,s:4,name:'Tryptophan (prescription)',dose:[{v:'500', l: ''},{v:'1000', l: ''},{v:'1500-2000', l: ''}],unit:'mg'},
        {id:41,s:4,name:'Zinc',dose:[{v:'15', l: ''},{v:'30', l: ''},{v:'45', l: ''}],unit:'mg'}];


    $scope.totalTrueInStage = function(stage_id) {
        var total = 0;
        for(var i in $scope.questions){
            var question = $scope.questions[i];
            if(question.s == stage_id && question.a == 1){
                total++;
            }
        }
        return total;
    }

    $scope.nextPage = function(){
        if($scope.currentCategory < ($scope.categories.length-1))
            $scope.currentCategory++;
        else
        {
            $scope.currentCategory = 0;
            $scope.currentStage++;
        }
    }

    $scope.previousPage = function(){

        if($scope.currentCategory === 0 && $scope.currentStage === 0)
            return;

        if($scope.currentCategory !== 0)
            $scope.currentCategory--;
        else
        {
            $scope.currentCategory = ($scope.categories.length-1);
            $scope.currentStage--;
        }
    }

    $scope.currentPage = function(){
        return (($scope.currentStage * $scope.categories.length) + $scope.currentCategory) +1;
    }

    $scope.goto = function(stageIndex,categoryIndex){
        $scope.currentStage = stageIndex;
        $scope.currentCategory = categoryIndex;
    }

    $scope.calcPage = function(stageIndex,categoryIndex){
        return ((stageIndex * $scope.categories.length) + categoryIndex) +1;
    }

    $scope.onResultsPage = function() {
        return ($scope.currentStage >= $scope.stages.length);
    }

    $scope.isComplete = function(stage_id,category_id){

        for(var i in $scope.questions){
            var question = $scope.questions[i];
            if(question.s == stage_id && question.c == category_id){
                if(question.a == null) return false
            }
        }
        return true;
    }

    $scope.getProgressPercentage = function() {
        return ($scope.currentPage() / $scope.totalPages) * 100;
    }

    $scope.gotoResults = function(){
        $scope.goto($scope.stages.length,0);
    }
});